export const filtersInitialState = {
  currentPage: 1,
  material: 'All',
};
export const notificationInitialState = {
  message: '',
  severity: 'info',
  open: false,
};
export const cartInitialState = {
  cart: [],
};
