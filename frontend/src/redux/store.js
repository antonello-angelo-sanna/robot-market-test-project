import { combineReducers, createStore } from 'redux';
import cart from './reducers/cart.reducer';
import filters from './reducers/filters.reducer';
import notifications from './reducers/notifications.reducer';

const rootReducer = combineReducers({
  cart,
  filters,
  notifications,
});

const store = createStore(rootReducer);
export default store;
