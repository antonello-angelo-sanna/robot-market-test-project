import { produce } from 'immer';
import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART,
} from '../types/cart.types';
import { cartInitialState } from '../initialState';
/**
 * @param { object } state
 * @param { object } action
 * @returns { object } state
 */
export default (state = cartInitialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return produce(state, (draftState) => {
        const robot = action.payload;
        const { cart } = draftState;
        const index = cart.findIndex((ele) => ele.name === robot.name);
        if (index < 0 && robot.stock > 0) {
          cart.push({ ...robot, quantity: 1 });
        } else if (robot.stock > cart[index].quantity) {
          cart[index].quantity += 1;
        }
        draftState.cart = cart;
        return draftState;
      });
    case REMOVE_FROM_CART:
      return produce(state, (draftState) => {
        const name = action.payload;
        return {
          ...draftState,
          cart: draftState.cart.filter((cartItem) => cartItem.name !== name),
        };
      });
    case UPDATE_CART:
      return produce(state, (draftState) => {
        const { cart } = draftState;
        const { robotName, quantity, operation } = action.payload;
        const cartItemIndex = cart.findIndex((ele) => ele.name === robotName);
        if (cartItemIndex < 0) {
          return draftState;
        }
        switch (operation) {
          case '+':
            cart[cartItemIndex].quantity += 1;
            break;
          case '-':
            cart[cartItemIndex].quantity -= 1;
            break;
          case 'set':
            cart[cartItemIndex].quantity = quantity;
            break;
          default:
            break;
        }
        return draftState;
      });
    default:
      return state;
  }
};
