import { produce } from 'immer';
import {
  SET_NOTIFICATIONS,
  CLOSE_NOTIFICATIONS,
} from '../types/notifications.types';
import { notificationInitialState } from '../initialState';
/**
 * @param { object } state
 * @param { object } action
 * @returns { object } state
 */
export default (state = notificationInitialState, action) => {
  switch (action.type) {
    case SET_NOTIFICATIONS:
      return produce(state, (draftState) => {
        const { message, severity } = action.payload;
        draftState.message = message;
        draftState.severity = severity;
        draftState.open = true;
        return draftState;
      });
    case CLOSE_NOTIFICATIONS:
      return produce(state, (draftState) => {
        draftState.open = false;
        return draftState;
      });
    default:
      return state;
  }
};
