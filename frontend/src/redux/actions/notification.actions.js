import {
  SET_NOTIFICATIONS,
  CLOSE_NOTIFICATIONS,
} from '../types/notifications.types';
/**
 * @param { object }
 * @returns { object }
 */
export const setNotification = ({ message, severity }) => {
  return {
    type: SET_NOTIFICATIONS,
    payload: {
      message,
      severity,
    },
  };
};
/**
 * @returns { object }
 */
export const closeNotifications = () => {
  return {
    type: CLOSE_NOTIFICATIONS,
  };
};
