import { SET_FILTERS } from '../types/filters.types';
/**
 *
 * @param { object } filters
 * @returns { object }
 */
export const setFilters = (filters) => {
  return {
    type: SET_FILTERS,
    payload: filters,
  };
};
