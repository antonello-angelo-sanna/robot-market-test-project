import React, { useEffect, useState } from 'react';
import Robots from './robots/robots';
import { uniqueArray } from '../functions/remove_duplicates';
import Filters from './robots/filters';
import Cart from './cart/cart';
import Pagination from './pagination';
import { connect } from 'react-redux';
import { setNotification } from '../redux/actions/notification.actions';
import Notification from './notifications';
import PropTypes from 'prop-types';
const { REACT_APP_BASE_URL: BASE_URL } = process.env;

/**

   * @param { array } robots
   * @param { number } currentPage
   * @param { string} material
   * @returns { object }
   */
const setCurrentRobots = (robots = [], currentPage, material) => {
  if (robots.length < 1) return robots;
  const botsPerPage = 10;
  const bots = [];
  const filteredBots = (robots = robots.filter(
    (bot) => bot.material === material || material === 'All'
  ));
  const start = botsPerPage * (currentPage - 1);
  const end = botsPerPage + start;
  for (let i = start; i < end; i++) {
    bots.push(filteredBots[i]);
  }
  return {
    currentBots: bots,
    botsCount: filteredBots.length,
  };
};

const Main = ({ currentPage, material, setNotification }) => {
  const [robots, setRobots] = useState([]);
  /**
   * Fetch robots and add them to the state
   * @returns void
   */
  const fetchRobots = () => {
    const url = `${BASE_URL}/api/robots`;
    fetch(url)
      .then((response) => {
        if (response && response.ok) {
          response.json().then((responseJson) => {
            setRobots(responseJson.data);
          });
        }
      })
      .catch((error) => {
        setNotification({
          message: `Something went wrong fetching the robots`,
          severity: 'error',
        });
      });
  };
  /**
   * Execute on component mount
   */
  useEffect(() => {
    fetchRobots();
  }, []);

  const { currentBots, botsCount } = setCurrentRobots(
    robots,
    currentPage,
    material
  );
  return (
    <div>
      <Notification />
      <section className="top_section">
        <Filters
          materials={uniqueArray(robots.map(({ material }) => material))}
        />
        <Pagination
          pageCount={Math.ceil(botsCount / 10)}
          botsCount={botsCount}
        />
      </section>
      <div className="body">
        <div className="products_list">
          <Robots robots={currentBots} />
        </div>
        <Cart />
      </div>
    </div>
  );
};
const mapStateToProps = ({ filters }) => {
  return filters;
};
Main.propTypes = {
  currentPage: PropTypes.number.isRequired,
  material: PropTypes.string.isRequired,
  setNotification: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { setNotification })(Main);
