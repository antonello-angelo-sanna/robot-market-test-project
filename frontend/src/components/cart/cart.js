import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import SideBar from '../sidebar';
import CartItem from './cartItem';
import { connect } from 'react-redux';
import uniqid from 'uniqid';
import { Typography } from '@material-ui/core';
import { moneyFormatter } from '../../functions/formatter';
/**
 *
 * @param { array } cart
 * @returns { number }
 */
const calcCartTotal = (cart) => {
  let total = 0;
  for (let cartItem of cart) {
    total += cartItem.quantity * cartItem.price;
  }
  return moneyFormatter(total);
};
export const cartItemsCount = (cart = []) => {
  if (!Array.isArray(cart)) {
    return [];
  }
  let itemCount = 0;
  cart.forEach((item) => {
    itemCount += item.quantity;
  });
  return itemCount;
};

const Cart = ({ cart }) => {
  return (
    <div className="cart">
      <SideBar cartItemsCount={cartItemsCount(cart)}>
        <List>
          {cart.map((cartItem) => (
            <CartItem key={uniqid()} {...cartItem} />
          ))}
        </List>
        <div className="cart_footer">
          <Typography variant="subtitle1">
            {cart.length > 0 && `Total: ${calcCartTotal(cart)}`}
          </Typography>
        </div>
      </SideBar>
    </div>
  );
};
Cart.propTypes = {
  cart: PropTypes.array.isRequired,
};
Cart.defaultProps = {
  cart: [],
};
const mapStateToProps = ({ cart }) => {
  return cart;
};
export default connect(mapStateToProps)(Cart);
