import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { moneyFormatter } from '../../functions/formatter';
import { IconButton } from '@material-ui/core';
import CartQuantityButtons from './cartQuantityButtons';
import { HighlightOffSharp } from '@material-ui/icons';
import PropTypes from 'prop-types';
import { removeFromCart } from '../../redux/actions/cart.actions';
import { useConfirm } from 'material-ui-confirm';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

const CartItem = ({ name, price, image, quantity, removeFromCart }) => {
  const confirm = useConfirm();
  const classes = useStyles();
  const handleRemoveFromCart = () => {
    confirm({ description: `Item will be delete from cart` })
      .then(() => {
        removeFromCart(name);
      })
      .catch(() => {
        return;
      });
  };
  return (
    <React.Fragment>
      <ListItem alignItems="center">
        <ListItemAvatar>
          <Avatar
            alt={name}
            src={image}
            style={{ width: 100, height: 'auto' }}
          />
        </ListItemAvatar>
        <ListItemText
          className="cart-item-name"
          primary={name}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
              >
                {moneyFormatter(price * quantity)}
              </Typography>
            </React.Fragment>
          }
        />
        <CartQuantityButtons
          quantity={quantity}
          name={name}
          handleRemoveFromCart={handleRemoveFromCart}
        />
        <IconButton onClick={handleRemoveFromCart}>
          <HighlightOffSharp />
        </IconButton>
      </ListItem>
      <Divider variant="inset" component="li" />
    </React.Fragment>
  );
};
CartItem.propsTypes = {
  removeFromCart: PropTypes.func.isRequired,
  updateCart: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image: PropTypes.string,
  quantity: PropTypes.number.isRequired,
};

export default connect(null, { removeFromCart })(CartItem);
