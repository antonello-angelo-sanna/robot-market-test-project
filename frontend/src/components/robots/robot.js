import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { dateFormatter, moneyFormatter } from '../../functions/formatter';
import { ShoppingCartSharp } from '@material-ui/icons';
import { connect } from 'react-redux';
import { addToCart } from '../../redux/actions/cart.actions';
import { setNotification } from '../../redux/actions/notification.actions';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));
/**
 *
 * @param { array } cart
 * @param { object } robot
 * @returns boolean
 */
export const isItemInStock = (cart, robot) => {
  if (cart.length < 1) return true;
  const item = cart.find((ele) => ele.name === robot.name);
  /**
   * If it finds the item in the cart, it checks that the stock quantity is greater than what's in the cart
   */
  if (item && !isNaN(item.quantity)) {
    return item.quantity < robot.stock;
  }
  /**
   * If the item is not in the cart, it just make sure that is in stock, and that the max items is respected.
   */
  return robot.stock > 0;
};

const RobotItem = ({ robot, addToCart, cart, setNotification }) => {
  /**

   * @returns {void}
   */
  const addRobotToCart = () => {
    const cartItemIndex = cart.findIndex((ele) => ele.name === name);
    if (cart.length > 4 && cartItemIndex < 0) {
      setNotification({
        message: `YOU CAN'T ADD MORE THAN 5 DIFFERENT PRODUCTS`,
        severity: `error`,
      });
      return false;
    }
    setNotification({
      message: `ROBOT ${name} ADDED TO CART`,
      severity: `success`,
    });

    addToCart(robot);
  };
  const { name, price, createdAt, image, stock, material } = robot;
  const classes = useStyles();
  const isBtnDisabled = !isItemInStock(cart, robot);
  return (
    <Card className={classes.root}>
      <CardHeader title={name} subheader={`Material: ${material}`} />
      <Typography>Stock: {stock}</Typography>
      <CardMedia
        className={`${classes.media} robot-image`}
        image={image}
        title={name}
      />
      <CardContent>
        <Typography variant="subtitle1" component="p" className="product-price">
          {moneyFormatter(price)}
        </Typography>
        <Typography
          variant="subtitle2"
          component="p"
          className="product-created-at"
        >
          {`Born on ${dateFormatter(createdAt)}`}
        </Typography>
        <CardActions>
          <IconButton
            disabled={isBtnDisabled}
            aria-label="add to cart"
            onClick={addRobotToCart}
          >
            <ShoppingCartSharp className="add-to-cart" />
          </IconButton>
        </CardActions>
      </CardContent>
    </Card>
  );
};
const mapStateToProps = ({ cart, notifications }) => ({
  ...cart,
  ...notifications,
});
RobotItem.propTypes = {
  robot: PropTypes.object.isRequired,
  addToCart: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired,
};
export default connect(mapStateToProps, {
  addToCart,
  setNotification,
})(RobotItem);
