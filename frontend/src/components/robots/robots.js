import React from 'react';
import Robot from './robot';
import PropTypes from 'prop-types';
import uniqid from 'uniqid';
const Robots = ({ robots }) =>
  robots.map(
    (robot, i) =>
      robot && robot.stock > 0 && <Robot robot={robot} key={uniqid()} />
  );
Robots.propTypes = {
  robots: PropTypes.array.isRequired,
};
Robots.defaultProps = {
  robots: [],
};
export default Robots;
