import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import { closeNotifications } from '../redux/actions/notification.actions';
import { Alert } from '@material-ui/lab';
import PropTypes from 'prop-types';
const Notification = ({
  open,
  message,
  closeNotifications,
  severity = 'error',
}) => {
  /**
   *
   * @param { object } event
   * @param { string } reason
   * @returns { void }
   */
  const handleClose = (event, reason) => {
    reason !== 'clickaway' && closeNotifications();
  };
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={open}
      autoHideDuration={2000}
      onClose={handleClose}
      action={
        <Alert severity={severity}>
          {message}
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </Alert>
      }
    />
  );
};
const mapStateToProps = ({ notifications }) => {
  return notifications;
};
Notification.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  closeNotifications: PropTypes.func,
  severity: PropTypes.string,
};
export default connect(mapStateToProps, { closeNotifications })(Notification);
